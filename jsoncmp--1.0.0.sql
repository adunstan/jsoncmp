-- # Copyright (c) 2014, Andrew Dunstan

-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION json_cmp" to load this file. \quit

create function jsoneq(json, json) 
returns boolean 
strict immutable 
language internal 
as 'texteq';

create function jsonne(json, json) 
returns boolean 
strict immutable 
language internal 
as 'textne';

create function json_lt(json, json) 
returns boolean 
strict immutable 
language internal 
as 'text_lt';

create function json_gt(json, json) 
returns boolean 
strict immutable 
language internal 
as 'text_gt';

create function json_le(json, json) 
returns boolean 
strict immutable 
language internal 
as 'text_le';

create function json_ge(json, json) 
returns boolean 
strict immutable 
language internal 
as 'text_ge';


create operator = (leftarg = json, rightarg = json, 
                   procedure = jsoneq, commutator = = );

create operator <> (leftarg = json, rightarg = json, 
                   procedure = jsonne, commutator = <> );

create operator < (leftarg = json, rightarg = json, 
                   procedure = json_lt, commutator = > );

create operator > (leftarg = json, rightarg = json, 
                   procedure = json_gt, commutator = < );

create operator <= (leftarg = json, rightarg = json, 
                   procedure = json_le, commutator = >= );

create operator >= (leftarg = json, rightarg = json, 
                   procedure = json_ge, commutator = <= );

