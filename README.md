Json comparison functions and operators for PostgreSQL
======================================================

Postgres' inbuilt json type doesn't have comparison functions and operators. 
But sometimes you need them, and this extension provides them. 
All these functions and operators call
PostgreSQL's inbuilt text comparison functions. Since json is stored under
the hood in a way indistinguishable from the text type, this works just fine.
It's as if you had cast the json values to text and then compared them, but
without all the hassle of doing that.

The operators provided are:

* `=` 
* `<>` 
* `<` 
* `<=` 
* `>` 
* `>=`

The corresponding functions are:

* `jsoneq`
* `jsonne`
* `json_lt`
* `json_le`
* `json_gt`
* `json_ge`

Pros and Cons
-------------

It's not clear that textual comparison is wanted or correct for json.
Whitespace is significant, so ` '{}'` is greater than `'  {}'` and keys
are not sorted in any canonical order, so ` '{"a":1,"b":2}'` is less than
`'{"b":2,"a":1}'` whereas they should arguably really be equal, since they 
describe essentially the same thing.

On the other hand, this does allow json values to be indexed and sorted 
without messy type casts. This can be important for things like UNION queries,
and DISTINCT queries, as well as queries that more obviously rely on data
ordering.
