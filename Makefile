# Copyright (c) 2014, Andrew Dunstan

EXTENSION = jsoncmp
DATA = jsoncmp--1.0.0.sql
DOCS = jsoncmp.md

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
